
extends Sprite

# member variables here, example:
# var a=2
const ROTATION_SPEED = 2
const SHOOT_SPEED = 330
var corn_count = 10
var corn_scn = preload("res://corn.tscn")
var cooldown = false
var timer

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_fixed_process(true)
	set_process_input(true)
	randomize()
	randomize()
	timer = get_node("ShootTimer")
	timer.connect("timeout", self, "end_cooldown")

func _fixed_process(delta):
	var rotation = 0
	if Input.is_action_pressed("rotate_ccw"):
		rotation += ROTATION_SPEED
	if Input.is_action_pressed("rotate_cw"):
		rotation -= ROTATION_SPEED
	var delta_rot = rotation * delta
	if get_rot()+delta_rot < 1.85 and get_rot()+delta_rot > 0:
		rotate(delta_rot)

func _input(event):
	if event.is_action_pressed("shoot"):
		if not cooldown:
			shoot()
			cooldown = true
			timer.start()

func end_cooldown():
	cooldown = false

func random_direction():
	var r = 0.5*(randf() + randf())
	var vec = Vector2(r - 0.5, -1)
	return vec.normalized()

func shoot():
	for _ in range(corn_count):
		var corn = corn_scn.instance()
		corn.add_to_group("corns")
		get_node("../Corns").add_child(corn)
		var velocity = SHOOT_SPEED * random_direction()
		var spawn_point = get_node("SpawnPoint")
		velocity = spawn_point.get_global_transform().basis_xform(velocity)
		corn.set_linear_velocity(velocity)
		#corn.set_global_pos(spawn_point.get_global_pos())
		corn.set_global_transform(spawn_point.get_global_transform())
		get_node("SamplePlayer").play("cannon_launch")
