
extends Area2D

# member variables here, example:
# var a=2
const MAX_HEALTH = 300.0
const STARVE_SPEED = 10.0
export(ColorRamp) var color_ramp
var popup_scn = preload("res://score_popup.tscn")
var health
var eat_count

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	connect("body_enter", self, "_body_enter")
	set_fixed_process(true)
	reset()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func _body_enter(body):
	if body.is_in_group("corns"):
		eat(body)

func _fixed_process(delta):
	var ratio = float(health) / MAX_HEALTH
	var color = color_ramp.interpolate(ratio)
	get_node("Sprite").set_modulate(color)
	var speed = STARVE_SPEED + 0.01*eat_count
	health -= speed * delta
	if health <= 0:
		die()

func reset():
	eat_count = 0
	health = MAX_HEALTH
	get_node("Sprite").set_frame(0)
	set_fixed_process(true)
	set_monitorable(true)
	get_node("Sprite").set_modulate(Color(1,1,1))

func die():
	health = 0
	set_monitorable(false)
	set_fixed_process(false)
	var ratio = 0.15
	get_node("Sprite").set_frame(1)
#	get_node("Sprite").set_modulate(Color(ratio, ratio, ratio))

func eat(body):
	if health <= 0:
		# the plant has died already
		return
	#print("Eating ", body)
	# TODO: Score and stuff
	# assuming a Corn
	eat_count += 1
	var score_color = Color(1, 1, 1)
	var score = 0
	if body.state == "unpopped":
		score -= 10
		score_color = Color(1, 0.5, 0.1)
	elif body.state == "popped":
		score += 20
		score_color = Color(0.3, 1, 0.3)
	elif body.state == "burnt":
		score -= 20
		score_color = Color(1, 0.1, 0.1)
	health = clamp(health + score, 0, MAX_HEALTH)
	get_parent().get_parent().update_score(score)
	var sound_key = "slurp"
	if score < 0:
		sound_key = "yuck"
	get_node("../../SamplePlayer").play(sound_key)
	var popup = popup_scn.instance()
	var text = str(score)
	if score >= 0:
		text = "+" + str(score)
	popup.get_node("Label").set_text(text)
	popup.set_global_pos(body.get_global_pos())
	get_node("../..").add_child(popup)
	popup.start(score_color)
	body.queue_free()
