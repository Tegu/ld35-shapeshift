
extends Node2D

# member variables here, example:
# var a=2

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	var label = get_node("Label")
	var tween = get_node("Tween")
	tween.connect("tween_complete", self, "del")
	tween.interpolate_method(label, "set_pos",\
			Vector2(0, 0), Vector2(0, -20), 0.7,\
			Tween.TRANS_QUAD, Tween.EASE_OUT)

func start(color):
	var label = get_node("Label")
	label.add_color_override("font_color", color)
	var tween = get_node("Tween")
	tween.start()

func del(object, key):
	queue_free()
