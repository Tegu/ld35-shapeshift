
extends RigidBody2D

# member variables here, example:
# var a=2
var heat = 0
var state = "unpopped"
const POP_LIMIT = 0.5
const BURN_LIMIT = 1.2

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func microwave(delta):
	heat += delta
	if heat >= POP_LIMIT and state == "unpopped":
		# unpopped -> popped
		state = "popped"
		get_node("UnpoppedSprite").hide()
		get_node("PoppedSprite").show()
		get_node("UnpoppedShape").set_trigger(true)
		get_node("PoppedShape").set_trigger(false)
		get_node("../../SamplePlayer").play("pop")
	elif heat >= BURN_LIMIT and state == "popped":
		# popped -> burnt
		state = "burnt"
		var sprite = get_node("PoppedSprite")
		sprite.set_modulate(Color(0.2, 0.2, 0.2))
