
extends Area2D

# member variables here, example:
# var a=2
var anim
var waves

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_fixed_process(true)
	waves = get_node("Waves")
	anim = get_node("AnimationPlayer")
	anim.play("wave")

func _fixed_process(delta):
	var mouse_pos = get_viewport().get_mouse_pos()
	set_global_pos(mouse_pos)
	if Input.is_action_pressed("microwave"):
		var bodies = get_overlapping_bodies()
		for body in bodies:
			if body.is_in_group("corns"):
				# was a corn, initiate SHAPESHIFT!!
				body.microwave(delta)
		if not anim.is_playing():
			anim.play("wave")
			waves.show()
	elif anim.is_playing():
		anim.stop()
		waves.hide()
