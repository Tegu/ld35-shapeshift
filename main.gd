
extends Node2D

# member variables here, example:
# var a=2
var score
var playing

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
#	restart()
	get_node("CornDeletionTimer").connect("timeout",\
			self, "free_sleeping_corns")
	get_node("LostPopup/CenterContainer/VBoxContainer/RestartButton").connect("pressed",\
			self, "restart")
	get_node("InitialPopup/CenterContainer/VBoxContainer/StartButton").connect("pressed",\
			self, "start")
#	set_process_input(true)
	set_fixed_process(true)
	get_node("StreamPlayer").play()
	stop()
	get_node("InitialPopup").show()

#func _input(event):
#	if event.is_action_pressed("pause"):
#		var popup = get_node("Popup")
#		if popup.is_hidden():
#			popup.popup_centered()
#			get_tree().set_pause(true)
#			popup.show()
#		else:
#			popup.hide()
#			get_tree().set_pause(false)
#		print("adf")

func start():
	get_node("InitialPopup").hide()
	restart()

func _fixed_process(delta):
	if playing and has_lost():
		lose()

func update_score(delta_score):
	#print("Updating score ", score, " with ", delta_score)
	score += delta_score
	get_node("HUD/Score").set_text("Score: " + str(score))

func has_lost():
	var plants = get_node("Plants").get_children()
	for plant in plants:
		if plant.health <= 0:
			return true
	return false

func free_sleeping_corns():
	for node in get_tree().get_nodes_in_group("corns"):
		if node.is_sleeping():
			# some visual feedback? *POOF* or something?
			node.queue_free()

func lose():
	#get_node("HUD/Text")
#	print("YOU LOST!")
	var popup = get_node("LostPopup")
#	popup.popup_centered()
	popup.show()
	stop()

func stop():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_node("Microwaver").set_fixed_process(false)
	get_node("Microwaver").hide()
	get_node("Cannon").set_process_input(false)
	get_node("Cannon").set_fixed_process(false)
	playing = false

func restart():
#	print("RESTARTING")
	score = 0
	update_score(0)
	var plants = get_node("Plants").get_children()
	for plant in plants:
		plant.reset()
	var corns = get_tree().get_nodes_in_group("corns")
	for corn in corns:
		corn.queue_free()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_node("Microwaver").show()
	get_node("Microwaver").set_fixed_process(true)
	get_node("Cannon").set_process_input(true)
	get_node("Cannon").set_fixed_process(true)
	get_node("LostPopup").hide()
	playing = true
